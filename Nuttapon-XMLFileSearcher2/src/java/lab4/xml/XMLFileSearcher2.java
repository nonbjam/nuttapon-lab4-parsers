/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.Scanner;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class XMLFileSearcher2 {

    public static void main(String argv[]) throws FileNotFoundException, XMLStreamException {
        String path = "keyword.txt";
        File filePath = new File("quotes.xml");
        boolean findQuote = false;
        boolean findWord = false;
        boolean findBy = false;
        String keyword = null;
        String word = null;
        String by = null;
        String name = null;
        Scanner scanner = new Scanner(new FileInputStream(path), "UTF-8");

        try {
            while (scanner.hasNextLine()) {
                keyword = scanner.nextLine();
                // Create object in class XMLInputFactory
                XMLInputFactory factory = XMLInputFactory.newInstance();
                // Create parser object in class XMLEventReader
                XMLEventReader r = factory.createXMLEventReader(new InputStreamReader(new FileInputStream(filePath)));

                // Iterate until there is no more date to read
                while (r.hasNext()) {
                    XMLEvent e = r.nextEvent();
                    //----- start -----//
                    if (e.isStartElement()) {
                        StartElement element = (StartElement) e;
                        name = element.getName().getLocalPart();
                        if (name.equals("quote")) {
                            findQuote = true;
                        }
                        if (findQuote && name.equals("word")) {
                            findWord = true;
                        }
                        if (findQuote && name.equals("by")) {
                            findBy = true;
                        }
                    }

                    //----- end -----//
                    if (e.isEndElement()) {
                        EndElement element = (EndElement) e;
                        name = element.getName().getLocalPart();
                        if (name.equals("quote")) {
                            findQuote = false;
                        }
                        if (findQuote && name.equals("word")) {
                            findWord = false;
                        }
                        if (findQuote && name.equals("by")) {
                            findBy = false;
                        }
                    }

                    //----- search keyword in file -----//
                    if (e.isCharacters()) {
                        Characters characters = (Characters) e;
                        if (findBy) {
                            by = characters.getData();
                            if (by.toLowerCase().contains(keyword.toLowerCase())) {
                                System.out.println(word + " by " + by);
                            }
                        }
                        if (findWord) {
                            word = characters.getData();
                        }
                    }
                }
            }

        } finally {
            scanner.close();
        }
    }
}