/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4.xml;

import java.io.*;
import java.net.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.xml.stream.*;
import javax.xml.stream.events.*;

@WebServlet(name = "FeedSearcher2", urlPatterns = {"/Nuttapon-FeedSearcher2"})
public class FeedSearcher2 extends HttpServlet {

    XMLEventReader r;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        String inputURL = request.getParameter("url");
        String inputKeyword = request.getParameter("keyword");
        boolean findTitle = false;
        boolean findLink = false;
        boolean findItem = false;
        String storeTitle = null;
        String storeLink = null;
        String checkName = null;

        try {
            URL u = new URL(inputURL);
            InputStream in = u.openStream();
            XMLInputFactory factory = XMLInputFactory.newInstance();
            r = factory.createXMLEventReader(in);
            out.print(
                    "<html><body><table border = '1'><tr ><th>Title</th><th>Link</th ></tr>");
            while (r.hasNext()) {
                XMLEvent e = r.nextEvent();
                if (e.isStartElement()) {
                    StartElement element = (StartElement) e;
                    checkName = element.getName().getLocalPart();
                    if (checkName.equals("item")) {
                        findItem = true;
                    }
                    if (findItem && checkName.equals("link")) {
                        findLink = true;
                    }
                    if (findItem && checkName.equals("title")) {
                        findTitle = true;
                    }
                }
                if (e.isEndElement()) {
                    EndElement element = (EndElement) e;
                    checkName = element.getName().getLocalPart();
                    if (checkName.equals("item")) {
                        findItem = false;
                    }
                    if (findItem && checkName.equals("link")) {
                        findLink = false;
                    }
                    if (findItem && checkName.equals("title")) {
                        findTitle = false;
                    }
                }
                if (e.isCharacters()) {
                    Characters characters = (Characters) e;
                    if (findTitle) {
                        storeTitle = characters.getData();
                    }
                    if (findLink) {
                        storeLink = characters.getData();
                        if (storeTitle.toLowerCase().contains(inputKeyword.toLowerCase())) {
                            out.print("<tr><td>");
                            out.print(storeTitle);
                            out.print("</td>");
                            out.print("<td>");
                            out.print("<a href='" + storeLink + "' >" + storeLink + "</a>");
                            out.print("</td></tr>");
                        }
                    }
                }
            }
            r.close();
            out.print("</table></body></html>");
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        } finally {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
