/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4.xml;

import java.io.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.*;

public class XMLFileWriter {

    public static void main(String argv[]) {
        new XMLFileWriter();
    }
    
    public XMLFileWriter() {
        try {
            DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbfac.newDocumentBuilder();           
            Document doc = docBuilder.newDocument();
            
            Element root = (Element) doc.createElement("quotes");
            doc.appendChild(root);
            
            /// ---------Jim Rohn-------- ///
            Element quote = doc.createElement("quote");
            root.appendChild(quote);
            Element word = doc.createElement("word");
            word.appendChild(doc.createTextNode("Time is more value than money. You can get more money, but you cannot get more time."));
            quote.appendChild(word);
            Element by = doc.createElement("by");
            by.appendChild(doc.createTextNode("Jim Rohn"));
            quote.appendChild(by);
            /// ---------Jim Rohn-------- ///

            /// ---------ว. วชิรเมธี-------- ///
            Element quote2 = doc.createElement("quote");
            root.appendChild(quote2);
            Element word2 = doc.createElement("word");
            word2.appendChild(doc.createTextNode("เมื่อทำอะไรสำเร็จ แม้จะเป็นก้าวเล็ก ๆ ของตัวเอง ก็ควรรู้จักให้รางวัลตัวเองบ้าง"));
            quote2.appendChild(word2);
            Element by2 = doc.createElement("by");
            by2.appendChild(doc.createTextNode("ว. วชิรเมธี"));
            quote2.appendChild(by2);
            /// ---------ว. วชิรเมธี-------- ///
            
            // ----- Tranformer ----- //
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer trans = tf.newTransformer();
            trans.setOutputProperty(OutputKeys.INDENT, "yes");
            
            StreamResult result = new StreamResult(new File("E:/Program Files/NetBeans 7.2.1/NetBeansProjects/Nuttapon-XMLFileWriter/testfile.xml"));
            DOMSource source = new DOMSource(doc);
            trans.transform(source, result);

            System.out.println("File saved!!");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
