/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4.xml;

import java.io.*;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

public class XMLFileWriter2 {
    
    public static void main(String argv[]) {
        
        try {
            //-------- read file --------//
            String filePath = "E:/Program Files/NetBeans 7.2.1/NetBeansProjects/Nuttapon-XMLFileWriter2/quotes1.xml";
            File file = new File(filePath);
            //-------- read file --------//
            
            XMLOutputFactory factory = XMLOutputFactory.newInstance();            
            XMLStreamWriter writer = factory.createXMLStreamWriter(new OutputStreamWriter(
                    new FileOutputStream(file)));
            
            //-------- config --------//
            writer.writeStartDocument("UTF-8", "1.0");            
            writer.writeStartElement("quotes"); 
            
            //-------- Jim Rohn --------//
            writer.writeStartElement("quote");
            
            writer.writeStartElement("word");
            writer.writeCharacters("Time is more value than money. You can get more money, but you cannot get more time.");
            writer.writeEndElement();
            
            writer.writeStartElement("by");
            writer.writeCharacters("Jim Rohn");
            writer.writeEndElement();
            
            writer.writeEndElement();
            //-------- Jim Rohn --------//
            
            //-------- ว. วชิรเมธี --------//
            writer.writeStartElement("quote");
            
            writer.writeStartElement("word");
            writer.writeCharacters("เมื่อทำอะไรสำเร็จ แม้จะเป็นก้าวเล็ก ๆ ของตัวเอง ก็ควรรู้จักให้รางวัลตัวเองบ้าง");
            writer.writeEndElement();
            
            writer.writeStartElement("by");
            writer.writeCharacters("ว. วชิรเมธี");
            writer.writeEndElement();
            
            writer.writeEndElement();
            //-------- ว. วชิรเมธี --------//
            
            writer.writeEndDocument();
            writer.flush();
            writer.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}