/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4.xml;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.*;

@WebServlet(name = "FeedWriterForm", urlPatterns = {"/Nuttapon-FeedWriterForm"})
public class FeedWriterForm extends HttpServlet {

    Document doc;
    String filePath = "E:/Program Files/NetBeans 7.2.1/NetBeansProjects/Nuttapon-FeedWriterForm/web/feed15.xml";
    File file = new File(filePath);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();

        try {
            
            if (file.exists()) {
                String rssTitle = request.getParameter("title");
                String rssUrl = request.getParameter("url");
                String rssDescription = request.getParameter("description");
                DocumentBuilderFactory builderFactory =
                        DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = builderFactory.newDocumentBuilder();
                doc = docBuilder.parse(file);
                FeedWriterForm fw = new FeedWriterForm();
                
                // Dom with document
                String feed = fw.updateRssTree(doc, rssTitle, rssUrl, rssDescription);
                out.println(feed);
            } else {
                String rssTitle = request.getParameter("title");
                String rssUrl = request.getParameter("url");
                String rssDescription = request.getParameter("description");
                DocumentBuilderFactory builderFactory =
                        DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = builderFactory.newDocumentBuilder();
                doc = docBuilder.newDocument();
                FeedWriterForm fw = new FeedWriterForm();
                
                // Dom with 
                String feed = fw.createRssTree(doc, rssTitle, rssUrl, rssDescription);
                out.print(feed);
            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public String createRssTree(Document doc, String rssTitle, String rssUrl, String rssDescription) throws Exception {

        Element rss = doc.createElement("rss");

        rss.setAttribute("version", "2.0");
        doc.appendChild(rss);

        Element channel = doc.createElement("channel");
        rss.appendChild(channel);

        Element title = doc.createElement("title");
        channel.appendChild(title);
        Text titleT = doc.createTextNode("Khon Kean University Rss Feed");
        title.appendChild(titleT);

        Element desc = doc.createElement("description");
        channel.appendChild(desc);
        Text descT = doc.createTextNode("Khon Kaen University Information News Rss Feed");
        desc.appendChild(descT);

        Element link = doc.createElement("link");
        channel.appendChild(link);
        Text linkT = doc.createTextNode("http://www.kku.ac.th");
        link.appendChild(linkT);

        Element lang = doc.createElement("lang");
        channel.appendChild(lang);
        Text langT = doc.createTextNode("en-th");
        lang.appendChild(langT);

        Element item = doc.createElement("item");
        channel.appendChild(item);

        Element iTitle = doc.createElement("title");
        item.appendChild(iTitle);
        Text iTitleT = doc.createTextNode(rssTitle);
        iTitle.appendChild(iTitleT);
        Element iDesc = doc.createElement("description");
        item.appendChild(iDesc);
        Text iDescT = doc.createTextNode(rssDescription);
        iDesc.appendChild(iDescT);
        Element iLink = doc.createElement("link");
        item.appendChild(iLink);
        Text iLinkT = doc.createTextNode(rssUrl);
        iLink.appendChild(iLinkT);
        Element pubDate = doc.createElement("pubDate");
        item.appendChild(pubDate);
        Text pubDateT = doc.createTextNode((new java.util.Date()).toString());
        pubDate.appendChild(pubDateT);

        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");


        StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        String xmlString = sw.toString();

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
        bw.write(xmlString);
        bw.flush();
        bw.close();
        return xmlString;
    }

    public String updateRssTree(Document doc, String rssTitle, String rssUrl, String rssDescription) throws Exception {
        Element channel = (Element) doc.getElementsByTagName("channel").item(0);

        Element item = doc.createElement("item");
        channel.appendChild(item);

        Element iTitle = doc.createElement("title");
        item.appendChild(iTitle);
        Text iTitleT = doc.createTextNode(rssTitle);
        iTitle.appendChild(iTitleT);

        Element iDesc = doc.createElement("description");
        item.appendChild(iDesc);
        Text iDescT = doc.createTextNode(rssDescription);
        iDesc.appendChild(iDescT);

        Element iLink = doc.createElement("link");
        item.appendChild(iLink);
        Text iLinkT = doc.createTextNode(rssUrl);
        iLink.appendChild(iLinkT);

        Element pubDate = doc.createElement("pubDate");
        item.appendChild(pubDate);
        Text pubDateT = doc.createTextNode((new java.util.Date()).toString());
        pubDate.appendChild(pubDateT);

        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");


        StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        String xmlString = sw.toString();

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
        bw.write(xmlString);
        bw.flush();
        return xmlString;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
 
}