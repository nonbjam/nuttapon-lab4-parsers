/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4.xml;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.xml.stream.*;
import javax.xml.stream.events.*;

@WebServlet(name = "FeedWriterForm", urlPatterns = {"/Nuttapon-FeedWriterForm2"})
public class FeedWriterForm2 extends HttpServlet {

    String filePath = "E:/Program Files/NetBeans 7.2.1/NetBeansProjects/Nuttapon-FeedWriterForm2/web/testfeed3.xml";
    File file = new File(filePath);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();

        try {
            String rssTitle = request.getParameter("title");
            String rssUrl = request.getParameter("url");
            String rssDescription = request.getParameter("description");
            // check file if have file return ture else return false
            if (file.exists()) {
                new FeedWriterForm2().updateRssFeed(rssTitle, rssDescription, rssUrl);
            } else {
                new FeedWriterForm2().createRssFeed(rssTitle, rssDescription, rssUrl);
            }
            out.println("<b <a href='http://localhost:8080/Nuttapon-FeedWriterForm2/testfeed.xml'>" + "Rss Feed</a> was create successfully</b>");
        } catch (Exception e) {
            System.out.println();
        }
    }

    private void createRssFeed(String rssTitle, String rssDescription, String rssUrl) throws Exception {
        XMLOutputFactory xof = XMLOutputFactory.newInstance();
        XMLStreamWriter xtw = xof.createXMLStreamWriter(new OutputStreamWriter(
                new FileOutputStream(file), "UTF-8"));
        xtw.writeStartDocument();
        
        xtw.writeStartElement("rss");
        xtw.writeAttribute("version", "2.0");
        xtw.writeStartElement("channel");
        
        xtw.writeStartElement("title");
        xtw.writeCharacters("Khon Kean University RSS Feed");
        xtw.writeEndElement();

        xtw.writeStartElement("description");
        xtw.writeCharacters("Khon Kean University Information News RSS Feed");
        xtw.writeEndElement();

        xtw.writeStartElement("link");
        xtw.writeCharacters("http://www.kku.ac.th");
        xtw.writeEndElement();

        xtw.writeStartElement("lang");
        xtw.writeCharacters("en~th");
        xtw.writeEndElement();
     
        xtw.writeStartElement("item");
        
        xtw.writeStartElement("title");
        xtw.writeCharacters(rssTitle);
        xtw.writeEndElement();
        
        xtw.writeStartElement("description");
        xtw.writeCharacters(rssDescription);
        xtw.writeEndElement();
        
        xtw.writeStartElement("link");
        xtw.writeCharacters(rssUrl);
        xtw.writeEndElement();
        
        xtw.writeStartElement("pubDate");
        xtw.writeCharacters((new java.util.Date()).toString());
        xtw.writeEndElement();
        
        xtw.writeEndElement();
        xtw.writeEndElement();
        xtw.writeEndElement();
        xtw.writeEndDocument();
        xtw.flush();
        xtw.close();
    }

    private void updateRssFeed(String rssTitle, String rssDescription, String rssUrl) throws Exception {
        XMLInputFactory xif = XMLInputFactory.newInstance();
        XMLEventReader feedReader = xif.createXMLEventReader(new InputStreamReader(new FileInputStream(file)));
        XMLOutputFactory xof = XMLOutputFactory.newInstance();
        XMLStreamWriter fWriter = xof.createXMLStreamWriter(new OutputStreamWriter(new FileOutputStream(file)));
        String elementName;

        while (feedReader.hasNext()) {
            XMLEvent e = feedReader.nextEvent();

            if (e.getEventType() == XMLEvent.START_DOCUMENT) {
                fWriter.writeStartDocument();
            }

            if (e.getEventType() == XMLEvent.START_ELEMENT) {
                StartElement element = (StartElement) e;
                elementName = element.getName().getLocalPart();
                if (elementName.equals("rss")) {
                    fWriter.writeStartElement("rss");
                    fWriter.writeAttribute("version", "2.0");
                } else {
                    fWriter.writeStartElement(elementName);
                }
            }

            if (e.isEndElement()) {
                EndElement element = (EndElement) e;
                elementName = element.getName().getLocalPart();
                if (elementName.equals("channel")) {
                    fWriter.writeStartElement("item");
                    fWriter.writeStartElement("title");
                    fWriter.writeCharacters(rssTitle);
                    fWriter.writeEndElement();
                    fWriter.writeStartElement("description");
                    fWriter.writeCharacters(rssDescription);
                    fWriter.writeEndElement();
                    fWriter.writeStartElement("link");
                    fWriter.writeCharacters(rssUrl);
                    fWriter.writeEndElement();
                    fWriter.writeStartElement("pubDate");
                    fWriter.writeCharacters((new java.util.Date()).toString());
                    fWriter.writeEndElement();
                    fWriter.writeEndElement();
                    fWriter.writeEndDocument();
                    fWriter.flush();
                    fWriter.close();
                    return;
                } else {
                    fWriter.writeEndElement();
                }
            }
            if (e.isCharacters()) {
                Characters characters = (Characters) e;
                fWriter.writeCharacters(characters.getData().toString());
            }
        }

        fWriter.flush();
        fWriter.close();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}